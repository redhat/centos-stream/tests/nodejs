<!--
SPDX-FileCopyrightText: Red Hat

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# NodeJS – Public test suite

This is the (still growing) public test suite for the NodeJS engine
in the Red Hat family of distributions (Fedora, CentOS Stream, and RHEL).

## Usage

This is a [tmt][]-strucured repository; read through the [docs][tmt-docs] for the detailed explanation of the various files and fields within them.

[tmt]: https://github.com/teemtee/tmt "Test Management Tool"
[tmt-docs]: https://tmt.readthedocs.io/en/stable/

To run the default test suite in Fedora Rawhide container on your machine,
run the following in the repository root:

```
tmt run
```

## Test structure

The top level directories (`smoke/`, `regression/`, …) group tests by topic. Preferably add new tests to existing ones, but feel free to create a new group if no appropriate exists yet.
Below the categories are the individual tests.

*   Very simple tests may be entirely self-contained within a single `.fmf` file. Example:
    ```
    smoke/
        example.fmf  ← contains all the metadata and test code
    ```

*   Most test should be in dedicated directory that contains all supporting files and the `main.fmf` file with metadata and instructions on how to run/use the other files. Example:
    ```
    smoke/
        example/
            test-script.mjs  ← the test code
            main.fmf  ← metadata
            ...  ← other supporting files
    ```

    The `main.fmf` above presumably makes use of the other files, for example:
    ```yaml
    # ... other metadata
    test: node test-script.mjs
    # ... other metadata
    ```

*   If needed, separate test cases may be provided for special circumstances (i.e. different distribution support).
    In that case, provide a `.fmf` file for each case as appropriate. It is also a good idea to provide default case (usually named `default.fmf`) to be run when no special case applies.
    ```
    smoke/
        example/
            main.fmf  ← common metadata, not considered a test case by tmt!
            default.fmf  ← the default case, can be (almost) empty
            rhel/  ← RHEL specific cases
                X.fmf
                Y.fmf
                rhel-specific-supporting-file.txt
    ```
