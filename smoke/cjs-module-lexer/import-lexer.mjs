// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import {strict as assert} from 'node:assert';
import lexer from 'node:internal/deps/cjs-module-lexer/dist/lexer';

await lexer.init();
assert.deepEqual(
	lexer.parse(''),
	{exports: [], reexports: []},
	'Empty module is parsed unexpectedly',
);
