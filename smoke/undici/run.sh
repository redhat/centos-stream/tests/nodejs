#!/bin/sh -e

# SPDX-FileCopyrightText: Red Hat
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# run local httpbin process for undici to call

export SERVER_HOSTNAME=localhost SERVER_PORT=8888

# === Setup ===

kill_server() {
  xargs -- kill --signal TERM --timeout 3000 KILL <server.pid && rm -f server.pid
}

${NODEJS_BIN:-node} ./server.mjs 2>server.log & echo $! >server.pid
trap kill_server EXIT

# === Test ===

${NODEJS_BIN:-node} --expose-internals ./import-undici.mjs
