// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import {strict as assert} from 'node:assert';
import undici from 'node:internal/deps/undici/undici';

const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER_PORT = process.env.SERVER_PORT || 80;

const response = await undici.fetch(`http://${SERVER_HOSTNAME}:${SERVER_PORT}/`);

assert.equal(response.status, 200, 'HTTP request was not successful');
assert.deepEqual(await response.json(), {'user-agent': 'node'}, 'Unexpected User-Agent');
