// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import http from 'node:http';

const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER_PORT = process.env.SERVER_PORT || 80;

http.createServer((request, response) => {
    const payload = {
        'user-agent': request.headers["user-agent"] || 'unknown',
    };

    response.setHeader("Content-Type", "application/json");
    response.writeHead(200);
    response.end(JSON.stringify(payload));
}).listen(SERVER_PORT, SERVER_HOSTNAME);
